import Foundation

protocol IGildedRose {
    var items: [Item] {get set}
    
    init(items: [Item])
    
    func updateQuality()
}
