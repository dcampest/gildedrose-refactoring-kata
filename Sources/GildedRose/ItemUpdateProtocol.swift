import Foundation

protocol ItemUpdateProtocol {
    func increaseQualityBy(value: Int)
    func decreaseQualityBy(value: Int)
    func decreaseSellInBy(value: Int)
}

extension Item: ItemUpdateProtocol {

    struct Limits {
        static var MAX_QUALITY = 50
        static var MIN_QUALITY = 0
    }

    func increaseQualityBy(value: Int) {
        guard self.quality < Limits.MAX_QUALITY  else { return }

        if self.quality + value < Limits.MAX_QUALITY {
            self.quality = self.quality + value
        } else {
            self.quality = Limits.MAX_QUALITY
        }

    }

    func decreaseQualityBy(value: Int) {
        guard self.quality > Limits.MIN_QUALITY  else { return }

        if self.quality - value > Limits.MIN_QUALITY {
            self.quality = self.quality - value
        } else {
            self.quality = Limits.MIN_QUALITY
        }
    }

    func decreaseSellInBy(value: Int) {
        self.sellIn = self.sellIn - value
    }

}
