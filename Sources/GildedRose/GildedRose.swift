public class GildedRose: IGildedRose {
    var items: [Item]
    
    required public init(items: [Item]) {
        self.items = items
    }
    
    public func updateQuality() {
        for item in items {
            
            switch item.name {
            case "Aged Brie":
                item.decreaseSellInBy(value: 1)
                
                switch item.sellIn {
                case ..<0:
                    item.increaseQualityBy(value: 2)
                case 0...:
                    item.increaseQualityBy(value: 1)
                default: break
                }
                
            case _ where item.name.hasPrefix("Backstage passes"):
                item.decreaseSellInBy(value: 1)
                
                switch item.sellIn {
                case ..<0:
                    item.quality = 0
                case 0..<5:
                    item.increaseQualityBy(value: 3)
                case 5..<10:
                    item.increaseQualityBy(value: 2)
                case 10...:
                    item.increaseQualityBy(value: 1)
                default: break
                }
                
            case _ where item.name.hasPrefix("Conjured"):
                item.decreaseSellInBy(value: 1)
                
                switch item.sellIn {
                case ..<0:
                    item.decreaseQualityBy(value: 4)
                case 0...:
                    item.decreaseQualityBy(value: 2)
                default: break
                }
                
                
            case "Sulfuras, Hand of Ragnaros": break
                
            default:
                item.decreaseSellInBy(value: 1)
                
                switch item.sellIn {
                case ..<0:
                    item.decreaseQualityBy(value: 2)
                case 0...:
                    item.decreaseQualityBy(value: 1)
                default: break
                }
                
            }
            
        }
    }
}
