@testable import GildedRose
import XCTest

class ItemTests: XCTestCase {

    func testItemInitialization() {
        let item = Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 10, quality: 49)

        XCTAssertEqual("Backstage passes to a TAFKAL80ETC concert", item.name)
        XCTAssertEqual(49, item.quality)
        XCTAssertEqual(10, item.sellIn)
    }

    func testItemDescription() {

        let item = Item(name: "+5 Dexterity Vest", sellIn: 10, quality: 20)

        XCTAssertEqual("+5 Dexterity Vest, 10, 20", item.description)
      }

      static var allTests: [(String, (ItemTests) -> () throws -> Void)] {
          return [
            ("testItemInitialization", testItemInitialization),
              ("testItemDescription", testItemDescription)
          ]
      }

}
