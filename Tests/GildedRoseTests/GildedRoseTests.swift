@testable import GildedRose
import XCTest

class GildedRoseTests: XCTestCase {
    
    func assert(item: Item, name: String, sellIn: Int, quality: Int) {
        XCTAssertEqual(name, item.name)
        XCTAssertEqual(sellIn, item.sellIn)
        XCTAssertEqual(quality, item.quality)
    }
    
    func assert(itemA: Item, itemB: Item) {
        XCTAssertEqual(itemA.name, itemB.name)
        XCTAssertEqual(itemA.quality, itemB.quality)
        XCTAssertEqual(itemA.sellIn, itemB.sellIn)
    }
    
    func generateItems() -> [Item] {
        let names = ["+5 Dexterity Vest",
                     "Aged Brie",
                     "Elixir of the Mongoose",
                     "Sulfuras, Hand of Ragnaros",
                     "Backstage passes to a TAFKAL80ETC concert",
                     "NewCustomType"]
        
        let res = names.flatMap { (name) -> [Item] in
            return (-100...100).flatMap { (sellIn) -> [Item] in
                return (-100...100).compactMap { (quality) -> Item in
                    return Item(name: name, sellIn: sellIn, quality: quality)
                }
            }
        }
        
        return res
    }
    
    func testGivenInput() {
        
        // arrange
        let items = [
            Item(name: "+5 Dexterity Vest", sellIn: 10, quality: 20),
            Item(name: "Aged Brie", sellIn: 2, quality: 0),
            Item(name: "Elixir of the Mongoose", sellIn: 5, quality: 7),
            Item(name: "Sulfuras, Hand of Ragnaros", sellIn: 0, quality: 80),
            Item(name: "Sulfuras, Hand of Ragnaros", sellIn: -1, quality: 80),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 15, quality: 20),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 10, quality: 49),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 5, quality: 49),]
            
        
        
        let app = GildedRose(items: items)
        
        // act
        app.updateQuality()
        
        // assert
        assert(item: items[0], name: "+5 Dexterity Vest", sellIn: 9, quality: 19)
        assert(item: items[1], name: "Aged Brie", sellIn: 1, quality: 1)
        assert(item: items[2], name: "Elixir of the Mongoose", sellIn: 4, quality: 6)
        assert(item: items[3], name: "Sulfuras, Hand of Ragnaros", sellIn: 0, quality: 80)
        assert(item: items[4], name: "Sulfuras, Hand of Ragnaros", sellIn: -1, quality: 80)
        assert(item: items[5], name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 14, quality: 21)
        assert(item: items[6], name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 9, quality: 50)
        assert(item: items[7], name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 4, quality: 50)

    }
    
    func testUpdateQuality_compareToOriginalImplementation() {
        
        // arrange
        let itemsA = generateItems()
        let itemsB = generateItems()
        let app: IGildedRose = GildedRose(items: itemsA)
        let originalApp: IGildedRose = GildedRoseOriginal(items: itemsB)
        
        // act
        app.updateQuality()
        originalApp.updateQuality()
        
        // assert
        for i in 0..<itemsA.count {
            assert(itemA: app.items[i], itemB: originalApp.items[i])
        }
    }
    
    
    func testConjuredItems() {
        
        // arrange
        let items = [
            Item(name: "Conjured Mana Cake", sellIn: 3, quality: 6),
            Item(name: "Conjured Mana Cake", sellIn: 0, quality: 6),
            Item(name: "Conjured Mana Cake", sellIn: -3, quality: 6),
            Item(name: "Conjured Something Else", sellIn: 3, quality: 6),
            Item(name: "Conjured", sellIn: -3, quality: 6),
            Item(name: "Conjured", sellIn: -3, quality: 0),
        ]
        
        
        let app: IGildedRose = GildedRose(items: items)
        
        
        // act
        app.updateQuality()
        
        // assert
        assert(item: items[0], name: "Conjured Mana Cake", sellIn: 2, quality: 4)
        assert(item: items[1], name: "Conjured Mana Cake", sellIn: -1, quality: 2)
        assert(item: items[2], name: "Conjured Mana Cake", sellIn: -4, quality: 2)
        assert(item: items[3], name: "Conjured Something Else", sellIn: 2, quality: 4)
        assert(item: items[4], name: "Conjured", sellIn: -4, quality: 2)
        assert(item: items[5], name: "Conjured", sellIn: -4, quality: 0)
        
    }
    
    static var allTests: [(String, (GildedRoseTests) -> () throws -> Void)] {
        return [
            ("testGivenInput", testGivenInput),
            ("testUpdateQuality_compareToOriginalImplementation", testUpdateQuality_compareToOriginalImplementation),
            ("testConjuredItems", testConjuredItems)
        ]
    }
}
