import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(GildedRoseTests.allTests),
        testCase(ItemTests.allTests)
    ]
}
#endif
